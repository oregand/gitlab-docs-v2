# GitLab Documentation Site

## Setup

Install dependencies:

```bash
yarn install
```

## Development

```bash
yarn dev
```

## Static Generation

This will create the `dist/` directory for publishing to static hosting:

```bash
yarn generate
```

To preview the static generated app, run `yarn start`

For detailed explanation on how things work, checkout [nuxt/content](https://content.nuxtjs.org).

## Commits

We use [Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/) for this Project.

```bash
<type>[optional scope]: <description>

[optional body]

[optional footer(s)]
```
