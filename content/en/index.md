---
title: Introduction
description: ''
position: 1
category: ''
features:
  - Feature 1
  - Feature 2
  - Feature 3
---

<img src="/hero-bg.svg" class="light-img" width="1280" height="640" alt=""/>
<img src="/hero-bg.svg" class="dark-img" width="1280" height="640" alt=""/>

[Module]() for [NuxtJS](https://nuxtjs.org).

<alert type="success">

Your documentation has been created successfully!

</alert>

Please choose a *version*:

<docs-multiselect :options="['VuSelf Hosted', '.com']"></docs-multiselect>

## Features

<list :items="features"></list>

<p class="flex items-center">Enjoy light and dark mode:&nbsp;<app-color-switcher class="inline-flex ml-2"></app-color-switcher></p>
